import random

#checking for random combinations for now

# below logic will first get the numebr of common letters between the guess word and the correct word, then filter out the main list based on required words only

# we stil need to implement the part of choosing a word from the list and checking to continue the game


import random

def read_five_letter_words(file_path):
    with open(file_path, 'r') as file:
        words = file.readlines()
    five_letter_words = [word.strip().lower() for word in words if len(word.strip()) == 5]
    return five_letter_words

def mastermind(correct_word, word_list):
    def get_rightChar_cnt(wordA, wordB):
        wordA_sorted = sorted(wordA)
        wordB_sorted = sorted(wordB)
        return sum(min(wordA_sorted.count(char), wordB_sorted.count(char)) for char in set(wordA))

    def filter_word_list(word_list, guess, correct_char_cnt):
        filter_list = []
        for word in word_list:
            if get_rightChar_cnt(word, guess) == correct_char_cnt:
                filter_list.append(word)
        return filter_list

    possible_words = word_list.copy()
    attempts = 0

    while True:
        guess = random.choice(possible_words)
        attempts += 1

        correct_letter_count = get_rightChar_cnt(correct_word, guess)
        print(f"Guess word: {guess}, Number of correct letters: {correct_letter_count}")

        if guess == correct_word:
            return f"' You win! {guess}' is the correct word, gueesed in {attempts} attempts!"
        if correct_letter_count == 0:
       		possible_words = [word for word in possible_words if not any(letter in word for letter in guess)]
        else:
        	possible_words = filter_word_list(possible_words, guess, correct_letter_count)

correct_word = "abaft"
words_list = 'words.txt'
five_letter_words = read_five_letter_words(words_list)

if correct_word in five_letter_words:
    result = mastermind(correct_word, five_letter_words)
    print(result)
else:
    print(f"The correct word '{correct_word}' is not a five-letter word or is not in the provided word list.")

