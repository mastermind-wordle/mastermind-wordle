import requests as req
import json
 

mm_url = "https://we6.talentsprint.com/wordle/apidocs/#/game/post_wordle_game_guess/"
register_url = "https://we6.talentsprint.com/wordle/game/register"
create_url = "https://we6.talentsprint.com/wordle/game/create"
guess_url = "https://we6.talentsprint.com/wordle/game/guess"
register_dict = {"mode" :"Mastermind" , "name" :"WE_BOT"}
register_with = json.dumps(register_dict)



session = req.Session()
resp = session.post(register_url , json = register_dict)

me=resp.json()['id']
creat_dict ={"id" :me , "overwrite" :True}
rc = session.post (create_url,json = creat_dict)


guess = {"id" : me , "guess" : guess}
correct = session.post(guess_url , json=guess)


