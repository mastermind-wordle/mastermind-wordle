import sys
import random

def get_random_word(file_path):
    with open(file_path, 'r') as file:
        words = file.readlines()

    words = [word.strip() for word in words if word.strip()]
    return random.choice(words)
   
file_path = sys.argv[1]
random_word = get_random_word(file_path)

if random_word:
    print(f"Random word: {random_word}")

