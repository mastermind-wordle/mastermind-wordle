import random

def read_five_letter_words(file_path):
    with open(file_path, "r") as file:
        words = file.readlines()
    five_letter_words = [
        word.strip().lower() for word in words if len(word.strip()) == 5
    ]
    return five_letter_words

def count_partial_matches(guess, hidden_word):
    guess_set = set(guess)
    hidden_set = set(hidden_word)
    return len(guess_set & hidden_set)




def filter_words(words, guess, partial_matches):
    filtered_words = []
    guess_set = set(guess)

    for word in words:
        if count_partial_matches(word, guess) == partial_matches:
            filtered_words.append(word)

    return filtered_words


def guess_word(words, hidden_word):
    guessed_word = random.choice(words)
    guesses = 1

    while guessed_word != hidden_word:
        partial_matches = count_partial_matches(guessed_word, hidden_word)
        print(f"Guess {guesses}: '{guessed_word}' ({partial_matches} partial matches)")

        if partial_matches == 0:

            words = [
                word
                for word in words
                if not any(letter in word for letter in guessed_word)
            ]

        if words:
            guessed_word = random.choice(words)
        else:
            break

        guesses += 1

    if guessed_word == hidden_word:
        print(f"Correctly guessed the word '{hidden_word}' in {guesses} guesses!")
    else:
        print("Unable to guess the word. No valid words left to guess.")



words_file = "words.txt"
five_letter_words = read_five_letter_words(words_file)

if five_letter_words:
    hidden_word = random.choice(five_letter_words)
    print("Hidden word has been chosen.")
    guess_word(five_letter_words, hidden_word)
else:
    print("No valid five-letter words found in the file.")

