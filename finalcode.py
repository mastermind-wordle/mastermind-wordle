import requests as req
import json
import random

register_url = "https://we6.talentsprint.com/wordle/game/register"
create_url = "https://we6.talentsprint.com/wordle/game/create"
guess_url = "https://we6.talentsprint.com/wordle/game/guess"
register_dict = {"mode": "Mastermind", "name": "WE_BOT"}

session = req.Session()
try:
    resp = session.post(register_url, json=register_dict)
    resp.raise_for_status()
    me = resp.json()['id']

    create_dict = {"id": me, "overwrite": True}
    rc = session.post(create_url, json=create_dict)

    def read_five_letter_words(file_path):
        with open(file_path, 'r') as file:
            words = file.readlines()
        five_letter_words = [word.strip().lower() for word in words if len(word.strip()) == 5]
        return five_letter_words

    def get_correct_letter_count(wordA, wordB):
        return sum((wordA[i] == wordB[i]) for i in range(len(wordA)))

    def filter_word_list(word_list, guess, correct_letter_count):
        filter_list = []
        for word in word_list:
            if get_correct_letter_count(word, guess) == correct_letter_count:
                filter_list.append(word)
        return filter_list

    def mastermind(word_list):
        possible_words = word_list.copy()
        attempts = 0

        while possible_words:
            guess = random.choice(possible_words)
            attempts += 1
            
            guess_payload = {"id": me, "guess": guess}
            correct = session.post(guess_url, json=guess_payload)
            correct.raise_for_status()
            feedback = correct.json()['feedback']
            message = correct.json()['message']
            
            if isinstance(feedback, int):
                correct_letter_count = feedback
            else:
                correct_letter_count = feedback['correct']
            
            print(f"Guess word: {guess}, Correct letter count: {correct_letter_count}, Message: {message}")
            
            if message == "win":
                return f"You win! '{guess}' is the correct word, guessed in {attempts} attempts."
            
            possible_words = filter_word_list(possible_words, guess, correct_letter_count)
            
            if not possible_words:
                return "No possible words left to guess. Something went wrong."

    words_list = 'words.txt'
    five_letter_words = read_five_letter_words(words_list)

    if five_letter_words:
        result = mastermind(five_letter_words)
        print(result)
    else:
        print("No five-letter words found in the provided file.")

except req.exceptions.RequestException as e:
    print(f"Error occurred: {e}")
